let key = "cbdb1f25866b842989356e653abdc925"
let token = "fc4aff7f90dc80d9c9d2d69d999967daaaaf17aa737f5e67cdbb81b27af72abe"

let idBoard = "623ae2aba00a4028d3ae6db4";

let val_input

let form = document.querySelector("form");
let input = document.querySelector("input");
let ul = document.querySelector("#form-data");


// function to create li 


function creatLi() {
    let li = document.createElement("li");
    let span = document.createElement("span")
    span.innerText = input.value;
    let div = document.createElement("div");
    div.classList.add("jsList")
    let inputBtn = document.createElement("input")
    inputBtn.classList.add("inputValue")
    inputBtn.setAttribute("type", "text")
    let button = document.createElement("button");
    button.classList.add("submit1");
    button.innerText = "ADD";
    let removeBtn = document.createElement("button");
    removeBtn.innerText = "REMOVE";

    li.appendChild(span)
    li.appendChild(div)
    li.appendChild(inputBtn)
    li.appendChild(button)
    li.appendChild(removeBtn);


    val_input = input.value
    return li;
}


// eventlistener for submiting form


let idObj = {};
form.addEventListener("submit", (event) => {
    event.preventDefault();
    let li = creatLi();

    liArray = []

    if (val_input === "") {
        alert("enter data")
    } else {
        ul.appendChild(li);
        fetch(`https://api.trello.com/1/lists?name=${event.target.children[0].value}&idBoard=${idBoard}&key=${key}&token=${token}`, {
                method: 'POST'
            })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => {
                let obj = JSON.parse(text)
                idObj[val_input] = obj["id"]
                console.log(idObj);
                // console.log(val_input)

            })
            .catch(err => console.error(err));


    }
    form.reset();
});
// const listCreation = () => {


// evendelg=egation for adding cards in to list and fetching data in to trello
let cardsObj = {}
let nameOfObj;
ul.addEventListener("click", (event) => {
    let btn = event.target;

    if (btn.innerText === "ADD") {
        let text = btn.previousSibling
        let textList = btn.previousSibling.previousSibling
        // textList.classList.add("cardContainer")
        console.log(textList)
        let p = document.createElement("p");
        p.classList.add("pop-up")

        p.innerText = text.value;

        textList.appendChild(p);

        drag();



        nameOfObj = btn.parentElement.firstElementChild.textContent
        console.log(nameOfObj)


        fetch(`https://api.trello.com/1/cards?name=${text.value}&idList=${idObj[nameOfObj]}&key=${key}&token=${token}`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json'
                }
            })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => {
                let object = JSON.parse(text)
                cardsObj[p.innerText] = object["id"]
                console.log(cardsObj)

            })
            .catch(err => console.error(err));

        text.value = "";
    }
});


// click event for removing the data and fetching same to trello


ul.addEventListener("click", (event) => {
    let btn = event.target;

    if (btn.innerText === "REMOVE") {
        btn.parentElement.remove()

        let nameOfObj1 = event.target.parentElement.firstElementChild.textContent
        // console.log(nameOfObj)

        fetch(`https://api.trello.com/1/lists/${idObj[nameOfObj1]}/closed?value=true&key=${key}&token=${token}`, {
                method: 'PUT'
            })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
        // .then(text => console.log(text))
        // .catch(err => console.error(err));
    }
})



let description = document.querySelector("#description");
let cardName;

// click event for pop-up window of description page

ul.addEventListener("click", (event) => {
    let btn = event.target;
    // ul.style.pointerEvents = "none"
    if (btn.className === "pop-up") {
        let li = document.createElement("li");

        let div2 = document.createElement("div");
        div2.classList.add("header-pop")
        let heading = document.createElement("h2");
        heading.textContent = btn.textContent;
        heading.classList.add("heading1");
        let removeDiscription = document.createElement("button");
        removeDiscription.textContent = "X";
        let heading1 = document.createElement("h3");
        heading1.textContent = "Description";
        let inputBtn1 = document.createElement("input");
        inputBtn1.type = "text";
        inputBtn1.classList.add("descriptionList")
        inputBtn1.placeholder = "Add description here";
        let saveBtn = document.createElement("button");
        saveBtn.textContent = "Save";
        saveBtn.classList.add("save1")
        let crossBtn = document.createElement("button");
        crossBtn.textContent = "Cancel";
        crossBtn.classList.add("cross-btn");
        let div3 = document.createElement("div");
        div3.classList.add("description-div");
        let heading2 = document.createElement("h3");
        heading2.textContent = "Activity";
        let div4 = document.createElement("div");
        div4.classList.add("comment-div");
        let inputBtn2 = document.createElement("input");
        inputBtn2.type = "text";
        inputBtn2.classList.add("commentList")
        inputBtn2.placeholder = "Add a comment";
        let saveBtn1 = document.createElement("button");
        saveBtn1.textContent = "Save";
        saveBtn1.classList.add("save2");
        let checkBox = document.createElement("input");
        checkBox.type = "checkbox";
        checkBox.classList.add("check-box");
        let watch = document.createElement("button");
        watch.textContent = "Watch";
        watch.classList.add("watch-btn");
        let div5 = document.createElement("div");
        div5.classList.add("comment-saving");


        div2.append(heading, removeDiscription);
        div3.append(saveBtn, crossBtn);
        div5.append(saveBtn1, checkBox, watch);
        li.append(div2, heading1, inputBtn1, div3, heading2, div4, inputBtn2, div5);


        description.append(li);
        cardName = heading.textContent




    }
});

// click event for removing the description page

description.addEventListener("click", (event) => {
    let btn = event.target;

    if (btn.innerText === "X") {
        btn.parentElement.parentElement.remove();
        // ul.style.pointerEvents = "all"
    }
})

// click event for adding description

description.addEventListener("click", (event) => {
    let btn = event.target;

    if (btn.className === "descriptionList") {
        btn.nextSibling.classList.remove("description-div");
        btn.nextSibling.classList.add("new-description-div");

    }
})

// click event for saving the description data and fetching the same to trello

description.addEventListener("click", (event) => {
    let btn = event.target;

    if (btn.className === "save1") {

        let secondChildElement = btn.nextSibling;
        let data = document.createElement("p");
        data.classList.add("data-description");
        data.textContent = btn.parentElement.previousSibling.value;
        btn.parentElement.previousSibling.style.display = "none";
        btn.parentElement.append(data);

        btn.remove();
        secondChildElement.remove();

        let descData = data.textContent

        fetch(`https://api.trello.com/1/cards/${cardsObj[cardName]}?key=${key}&token=${token}&desc=${descData}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json'
                }
            })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
        // .then(text => console.log(text))
        // .catch(err => console.error(err));
    }
})

// click event for saving comments and posting comments in trello using fetch

description.addEventListener("click", (event) => {
    let btn = event.target;

    if (btn.className === "save2") {
        let commentData = document.createElement("p");
        commentData.textContent = btn.parentElement.previousSibling.value;

        let textComment = btn.parentElement.previousSibling.value
        btn.parentElement.previousSibling.previousSibling.append(commentData);
        btn.parentElement.previousSibling.value = "";



        fetch(`https://api.trello.com/1/cards/${cardsObj[cardName]}/actions/comments?text=${textComment}&key=${key}&token=${token}`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json'
                }
            })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
        // .then(text=>console.log(text));

    }
})

// rendering ui the list


// getting data from board and rendering it on UI



async function listData() {
    let response = await fetch(`https://api.trello.com/1/boards/${idBoard}/lists?key=${key}&token=${token}`, {
        method: 'GET'
    })
    return response.json();
}
listData()
    .then(text => {
        let datas = text;
        for (let index in datas) {
            // console.log(datas[index].id)
            const li = document.createElement("li");
            li.classList.add("list")
            const span = document.createElement("span");
            span.textContent = datas[index].name
            span.setAttribute("id", datas[index].id)
            span.classList.add("span")
            // console.log(span)
            let div = document.createElement("div");
            div.classList.add("jsList")
            let inputBtn = document.createElement("input")
            inputBtn.classList.add("inputValue")
            inputBtn.setAttribute("type", "text")
            let button = document.createElement("button");
            button.classList.add("submit2");
            button.innerText = "ADD";
            let removeBtn = document.createElement("button");
            removeBtn.innerText = "REMOVE";

            li.appendChild(span)
            li.appendChild(div)
            li.appendChild(inputBtn)
            li.appendChild(button)
            li.appendChild(removeBtn);
            ul.append(li)

            console.log(datas)
            button.addEventListener("click", (e) => {

                let p = document.createElement("p");
                p.classList.add("pop-up")
                p.classList.add("draggable")
                p.setAttribute("draggable", true)
                p.innerText = inputBtn.value;
                console.log(p.innerText);


                // div.appendChild(p);


                // draggable code for after rendered page while clicking on add button


                drag();

                // posting again cards after redering  


                async function add() {
                    let response = await fetch(`https://api.trello.com/1/cards?name=${inputBtn.value}&idList=${datas[index].id}&key=${key}&token=${token}`, {
                        method: 'POST',
                    })
                    return response.json();
                }
                add()
                    .then(text => {
                        console.log(text)
                    })
                    .catch(err => console.error(err));
            })

            // eventlistener for removing list after rendering on UI

            removeBtn.addEventListener("click", (e) => {

                e.target.parentElement.remove();

                async function removing() {


                    let response = await fetch(`https://api.trello.com/1/lists/${datas[index].id}/closed?value=true&key=${key}&token=${token}`, {
                        method: 'PUT'
                    })
                    return response.json();
                }
                removing()
                // .then(text=> {
                //     console.log(text)
                // })
                // .catch(err=>console.log(err);
            })

            // getting the cards data from the lists

            async function getCards() {
                let response = await fetch(`https://api.trello.com/1/lists/${datas[index].id}/cards?key=${key}&token=${token}`, {
                    method: 'GET'
                })
                return response.json();
            }

            getCards()
                .then(text => {

                    let cards = text;
                    for (let card in cards) {
                        if (datas[index].id === cards[card].idList) {
                            // console.log("found");
                            let p = document.createElement("p");
                            p.classList.add("pop-up1");
                            p.classList.add("draggable");
                            p.setAttribute("draggable", true)
                            p.setAttribute("id", cards[card].id)
                            p.innerText = cards[card].name;
                            div.appendChild(p);


                            drag();



                            // getting the description page from cards

                            async function descriptData() {

                                let response = await fetch(`https://api.trello.com/1/cards/${cards[card].id}?key=${key}&token=${token}`, {
                                    method: "GET"
                                })
                                return response.json();
                            }

                            descriptData()
                                .then(text => {
                                    let descriptionData = text
                                    if (cards[card].id === descriptionData.id) {
                                        p.addEventListener("click", () => {
                                            let li = document.createElement("li");

                                            let div2 = document.createElement("div");
                                            div2.classList.add("header-pop")
                                            let heading = document.createElement("h2");
                                            heading.textContent = cards[card].name;
                                            heading.classList.add("heading1");
                                            let removeDiscription = document.createElement("button");
                                            removeDiscription.textContent = "X";
                                            let heading1 = document.createElement("h3");
                                            heading1.textContent = "Description";
                                            let p = document.createElement("p");
                                            p.classList.add("descData");
                                            p.textContent = descriptionData.desc
                                            let inputBtn1 = document.createElement("input");
                                            inputBtn1.type = "text";
                                            inputBtn1.classList.add("descriptionList")
                                            inputBtn1.placeholder = "Add description here";
                                            let saveBtn = document.createElement("button");
                                            saveBtn.textContent = "Save";
                                            saveBtn.classList.add("save1")
                                            let crossBtn = document.createElement("button");
                                            crossBtn.textContent = "Cancel";
                                            crossBtn.classList.add("cross-btn");
                                            let div3 = document.createElement("div");
                                            div3.classList.add("description-div");
                                            let heading2 = document.createElement("h3");
                                            heading2.textContent = "Activity";
                                            let div4 = document.createElement("div");
                                            div4.classList.add("comment-div");
                                            let inputBtn2 = document.createElement("input");
                                            inputBtn2.type = "text";
                                            inputBtn2.classList.add("commentList")
                                            inputBtn2.placeholder = "Add a comment";
                                            let saveBtn1 = document.createElement("button");
                                            saveBtn1.textContent = "Save";
                                            saveBtn1.classList.add("save2");
                                            let checkBox = document.createElement("input");
                                            checkBox.type = "checkbox";
                                            checkBox.classList.add("check-box");
                                            let watch = document.createElement("button");
                                            watch.textContent = "Watch";
                                            watch.classList.add("watch-btn");
                                            let div5 = document.createElement("div");
                                            div5.classList.add("comment-saving");

                                            div2.append(heading, removeDiscription);
                                            div3.append(saveBtn, crossBtn);
                                            div5.append(saveBtn1, checkBox, watch);
                                            li.append(div2, heading1, p, inputBtn1, div3, heading2, div4, inputBtn2, div5);

                                            description.append(li);
                                            if (p.textContent !== "") {
                                                inputBtn1.style.display = "none";
                                            }

                                            // updating again description after rendering the page 

                                            saveBtn.addEventListener("click", () => {
                                                let descData = inputBtn1.value;

                                                async function saving() {
                                                    let response = fetch(`https://api.trello.com/1/cards/${cards[card].id}?key=${key}&token=${token}&desc=${descData}`, {
                                                        method: 'PUT',

                                                    })
                                                    return response.json();
                                                }
                                                saving()
                                                    .then(text => console.log(text))
                                                    .catch(err => console.log(err))
                                            })

                                            // getting the comments data in cards

                                            async function commentData() {

                                                let response = await fetch(`https://api.trello.com/1/cards/${cards[card].id}/actions?key=${key}&token=${token}`, {
                                                    method: "GET"
                                                })
                                                return response.json();
                                            }

                                            commentData()
                                                .then(text => {
                                                    let descriptionData = text;

                                                    for (let comments1 in descriptionData) {

                                                        let commentsData = document.createElement("p");
                                                        commentsData.textContent = descriptionData[comments1].data.text;
                                                        div4.append(commentsData);
                                                    }
                                                })
                                                .catch(err => console.error(err));

                                            // posting again cards after rendered UI
                                            saveBtn1.addEventListener("click", () => {
                                                async function commentPosting() {
                                                    let response = await fetch(`https://api.trello.com/1/cards/${cards[card].id}/actions/comments?text=${inputBtn2.value}&key=${key}&token=${token}`, {
                                                        method: 'POST'
                                                    })
                                                    return response.json();
                                                }
                                                commentPosting()
                                                    .then(text => console.log(text))
                                                    .catch(err => console.log(err))


                                            })

                                        })
                                    }
                                })
                                .catch(err => console.error(err));

                        }
                    }


                });
        }

    })

// drag and drop

let cardID;
let listID;

function drag() {
    const containers = document.querySelectorAll('.jsList')
    const draggables = document.querySelectorAll('.draggable')
    // console.log(containers)
    draggables.forEach(draggable => {
        draggable.addEventListener('dragstart', dragStart)

    })
    containers.forEach(container => {
        container.addEventListener('dragover', dragOver);
        container.addEventListener('dragenter', dragEnter);
        container.addEventListener('dragleave', dragLeave);
        container.addEventListener('drop', dragDrop);
    });

    function dragOver(e) {
        e.preventDefault()
        console.log('drag over');

    }

    function dragEnter() {
        console.log('drag entered');
    }

    function dragLeave() {
        console.log('drag left');
    }

    function dragDrop() {
        console.log('drag dropped');
    }

    function dragStart(e) {
        console.log('drag started');
        dragItem = this;
        cardID = e.target.getAttribute("id")
    }

    function dragDrop(e) {
        console.log('drag dropped');
        this.append(dragItem);
        listID = e.target.parentElement.previousSibling.getAttribute("id")
        async function updatingDrag() {
            let response = await fetch(`https://api.trello.com/1/cards/${cardID}?pos=bottom&idList=${listID}&key=${key}&token=${token}`, {
                method: "PUT"
            })
            return response.json();
        }
        updatingDrag()
            .then(text => console.log(text))
    }
}